import './App.css';
import AgregarLibro from './fragments/AgregarLIbro';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import EditarLibro from './fragments/EditarLibro';
import PresentarLibro from './fragments/PresentarLibro';

function App() {
  return (
    <div className="App">
    <Routes>
      <Route path='/presentarLibro' element={<PresentarLibro/>}/>
      <Route path='/agregarlibro' element={<AgregarLibro/>}/>
      <Route path='/presentarLibro/editar' element={<EditarLibro/>}/>
    </Routes>
    </div>
  );
}

export default App;
