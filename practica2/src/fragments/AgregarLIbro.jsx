import React, { useState } from 'react';
import { Books, GuardarLibro } from '../hooks/Conexion';
import { useNavigate } from 'react-router';
import mensajes from '../utiles/Mensajes';
import { useForm } from 'react-hook-form';
import '../style/style.css';
function AgregarLibro() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const navegation = useNavigate();
    const [titulo, setTitulo] = useState([]);
    const [lltitulo, setLltitulo] = useState(false);
    const [nro, setNro] = useState(0);
    const [data, setData] = useState([]);
    const [llLIbros, llsetLibros] = useState(false);
    if (!llLIbros) {
        Books().then((info) => {
            var hola = info;
            if (info.error == true) {
                mensajes(info.mensajes);
            } else {
                setNro(hola.length);
            }  }) }
    const onSubmit = (data) => {
        var datos = {
            "id": nro + 1,
            "title": data.tiituloText,
            "description": data.descripcionText,
            "pageCount": data.pageCount,
            "excerpt": data.excerpt,
            "publishDate": data.anio
        };
        GuardarLibro(datos).then((info) => {
            if (info.error === true) {
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.message);
                navegation('/presentarLibro');   }    });
    }; const handlePageCountChange = (e) => {
        const value = e.target.value.replace(/\D/g, ''); // Elimina cualquier caracter que no sea un número
        e.target.value = value;
    };

    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    {/** INGRESAR MODELO */}
                                    <div className="form-group">
                                        <input type="text" {...register('tiituloText', { required: true })} className="form-control form-control-user" placeholder="Ingrese el titulo" />
                                        {errors.tiituloText && errors.tiituloText.type === 'required' && <div className='alert alert-danger'>Ingrese el titulo</div>}
                                    </div>
                                    {/** INGRESAR AÑO */}
                                    <div className="form-group">
                                        <input type="datetime-local" className="form-control form-control-user" placeholder="Ingrese el año" {...register('anio', { required: true })} />
                                        {errors.anio && errors.anio.type === 'required' && <div className='alert alert-danger'>Ingrese una fecha</div>}
                                    </div>
                                    {/** INGRESAR DESCRIPCION */}
                                    <div className="form-group">
                                        <input type="text" {...register('descripcionText', { required: true })} className="form-control form-control-user" placeholder="Ingrese la descripcion" />
                                        {errors.descripcionText && errors.descripcionText.type === 'required' && <div className='alert alert-danger'>Ingrese una descripcion</div>}
                                    </div>
                                    {/** INGRESAR pageCount */}
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el pageCount" {...register('pageCount', { required: true })} onChange={handlePageCountChange} />
                                        {errors.pageCount && errors.pageCount.type === 'required' && <div className='alert alert-danger'>Ingrese el pageCount</div>}
                                    </div>
                                    {/** INGRESAR excerpt */}
                                    <div className="form-group">
                                        <input type="text" {...register('excerpt', { required: true })} className="form-control form-control-user" placeholder="Ingrese el excerpt" />
                                        {errors.excerpt && errors.excerpt.type === 'required' && <div className='alert alert-danger'>Ingrese una excerpt</div>}
                                    </div>
                                    {/** BOTÓN CANCELAR */}
                                    <div style={{ display: 'flex', gap: '10px' }}>
                                        <a href="/presentarLibro" className="btn btn-danger btn-rounded">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                            </svg>
                                            <span style={{ marginLeft: '5px' }}>Cancelar</span>
                                        </a>

                                        {/** BOTÓN REGISTRAR */}
                                        <input className="btn btn-success btn-rounded" type='submit' value='Registrar'></input>
                                    </div>

                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AgregarLibro;