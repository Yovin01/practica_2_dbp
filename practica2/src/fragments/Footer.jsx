const Footer = () => {
    return ( 
        <footer className="sticky-footer bg-white">
                <div className="container my-libro">
                    <div className="copyright text-center my-auto">
                        <span>Copyright ©. Yovin001</span>
                    </div>
                </div>
            </footer>
     );
}
 
export default Footer;