import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Input } from 'react-bootstrap';
import React, { useState } from 'react'
import { Books } from '../hooks/Conexion'
import AgregarLibro from "./AgregarLIbro";
import DataTable from "react-data-table-component";
import { useNavigate } from "react-router";
import { getID, saveID } from "../utiles/ides";
const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
const columns = [
    {
        name: 'Title',
        selector: row => row.title,
        wrap: true,
    },
    {
        name: 'Description',
        selector: row => row.description,
        wrap: true,
    },
    {
        name: 'PageCount',
        selector: row => row.pageCount,
        wrap: true,
    },
    {
        name: 'Excerpt',
        selector: row => row.excerpt,
        wrap: true,
    },
    {
        name: 'PublishDate',
        selector: row => row.publishDate,
        wrap: true,
    },
    {
        /*   name: 'Acciones',
           selector: row => (<>
         
               <div style={{ display: 'flex', gap: '10px' }}>
                   <a href="/crudlibro/editar" class="btn btn-outline-info btn-rounded">
                       <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                           <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                           <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                       </svg>
                   </a>
   
                   <a href="/crudlibro" className="btn btn-outline-danger btn-rounded">
                       <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                           <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                           <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                       </svg>
                   </a>
               </div>
               
           </>),*/
    },

];


export const PresentarLibro = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [data, setData] = useState([]);
    const navegation = useNavigate();
    const [llLIbros, llsetLibros] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const [selectedBook, setSelectedBook] = useState(null);
    const [numeroFila, setNumeroFila] = useState(0);
    if (!llLIbros) {
        Books().then((info) => {
            var tdatos = info;
            setData(tdatos);
            console.log(tdatos);
            llsetLibros(true);
        })
    }

    const handleBookClick = (book) => {
        setSelectedBook(book);
        setNumeroFila(book.id);
        saveID(book.id);
        console.log(getID());
        handleShow(true);
        navegation('/presentarLibro/editar');

    };
    const handleSearch = (e) => {
        const searchValue = e.target.value;
        setSearchText(searchValue);
        const filteredBooks = data.filter(book =>
            book.title.toLowerCase().includes(searchValue.toLowerCase()));
        setFilteredData(filteredBooks);
    };

    return (
        <div className="container">
            <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                <div className="row ">

                    <div className="col-sm-3 mt-5 mb-4 text-gred">
                        <div className="search">
                            <form className="form-inline">
                                <input
                                    className="form-control mr-sm-2"
                                    type="search"
                                    placeholder="Buscar Libro"
                                    aria-label="Search"
                                    value={searchText}
                                    onChange={handleSearch} />
                            </form>
                        </div>
                    </div>
                    <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ color: "blue" }}>
                        <h2><b>Detalles de libros</b></h2></div>
                    <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                        <Button variant="primary" onClick={handleShow}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1
                                 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                            </svg>
                            <span style={{ marginLeft: '5px' }}>Agregar libro</span>
                        </Button>
                    </div>
                </div>
                <div className="row">

                    <DataTable
                        columns={columns}
                        data={data}
                        selectableRows
                        onRowClicked={handleBookClick}
                    /> <div className="model_box">
                        <Modal
                            show={show}
                            onHide={handleClose}
                            backdrop="static"
                            keyboard={false}
                        > <Modal.Body>
                            </Modal.Body>
                            <Modal.Header closeButton>
                                <Modal.Title>Agregar libro//</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <AgregarLibro />
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cerrar
                                </Button>

                            </Modal.Footer>
                        </Modal>
   </div> </div></div>  </div>
    );}
export default PresentarLibro;

