const URL= "https://fakerestapi.azurewebsites.net/api/v1"

export const Books = async () => {
    const datos = await (await fetch(URL + "/Books", {
        method: "GET",
    })).json();
 //   console.log(datos);
    return datos;
}

export const ObtenerBook = async (id) => {
    const datos = await (await fetch(URL + "/Books/"+id, {
        method: "GET",
    })).json();
   console.log(datos);
    return datos;
}

export const GuardarLibro = async (data) => {
var cabecera = {"Content-Type" : "application/json"};
    const datos = await (await fetch(URL + "/Books", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
   console.log(datos);
    return datos;
}

export const ModificarLibro = async (data, id) => {
    var cabecera = {"Content-Type" : "application/json"};
    const datos = await (await fetch(URL + "/Books/"+id, {
        method: "PUT",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    console.log(datos);
    return datos;
}
